﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.DataAccess
{
    public class DataHelper
    {
        FleetManagementSystemDBEntities db = new FleetManagementSystemDBEntities();

        public bool AddAssignment(DriverAssignment a)
        {
            var assign = AssignmentMapper.AccessToDB(a);
            bool co = false;
            if(assign.Complete > 0)
            {
                co = true;
            }
            try
            {
                db.sp_addassignment(assign.DriverFK, assign.Details, assign.Addr1, assign.Addr2, co);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool updateC(DriverAssignment d)
        {
            var a = AssignmentMapper.AccessToDB(d);
            bool co = false;
            if(d.complete > 0)
            {
                co = true;
            }
            try
            {
                db.sp_updateCompleteStatus(d.aId,co);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Drivr> GetDriverList()
        {
            var dList = db.Drivers.ToList();
            var drivrs = DriverList(dList);
            return drivrs;
        }

        private List<Drivr> DriverList(List<Driver> dList)
        {
            var newList = new List<Drivr>();
            foreach(var item in dList)
            {
                newList.Add(new Drivr()
                {
                    Id = item.DriverId,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    handle = item.handle,
                    pass = item.pass
                });
            }
            return newList;
        }

        public List<DriverAssignment> GetAssignList()
        {
            var aList = db.Assignments.ToList();
            var assignments = AssignList(aList);
            return assignments;
        }

        private List<DriverAssignment> AssignList(List<Assignment> aList)
        {
            var newList = new List<DriverAssignment>();
            foreach(var item in aList)
            {
                newList.Add(new DriverAssignment()
                {
                    aId = item.AssignmentId,
                    driverId = item.DriverFK,
                    details = item.Details,
                    sAddress = item.Addr1,
                    eAddress = item.Addr2,
                    complete = item.Complete,
                    drivr = item.Driver
                });
            }
            return newList;
        }
    }
}
