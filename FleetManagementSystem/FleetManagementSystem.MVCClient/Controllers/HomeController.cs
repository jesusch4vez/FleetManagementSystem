﻿using FleetManagementSystem.MVCClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using domain = FleetManagementSystem.Domain;

namespace FleetManagementSystem.MVCClient.Controllers
{
    public class HomeController : Controller
    {

        private domain.DomainHelper dh = new domain.DomainHelper();

        // GET: Home
        public ActionResult Index()
        {
            Drivr d = new Drivr();
            return View("Index","_Layout2", d);
        }

        public ActionResult Login(Drivr d)
        {
            var e = new domain.DriverDTO()
            {
                Id = d.Id,
                handle = d.handleProp,
                pass = d.passProp
            };
            if (dh.login(e))
            {
                return RedirectToAction("Assignment", "Driver",e);
            }
            else
            {
                TempData["alert"] = "Login Failed!";
                return View("Login", "_Layout2", d);
            }
        }

        public ActionResult LoginInit()
        {
            var e = new Drivr();

            return View("Login", "_Layout2", e);
        }
    }
}