﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FleetManagementSystem.MVCClient.Models;
using FleetManagementSystem.DataAccess;

namespace FleetManagementSystem.MVCClient.ViewModels
{
    public class Drivr
    {
        public int Id { get; set; }

        [DataType(DataType.Text)]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "User Name")]
        public string handle { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Password")]
        public string pass { get; set; }

        public string handleProp { get; set; }
        public string passProp { get; set; }

        public virtual IEnumerable<Assignment> assignments { get; set; }
    }
}