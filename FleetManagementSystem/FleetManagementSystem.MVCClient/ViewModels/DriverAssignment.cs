﻿using FleetManagementSystem.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetManagementSystem.MVCClient.ViewModels
{
    public class DriverAssignment
    {
        public int aId { get; set; }
        public int driverId { get; set; }

        [DataType(DataType.Text)]
        [Display(Name ="Details")]
        public string details { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Starting Address")]
        public string sAddress { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Ending Address")]
        public string eAddress { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Complete?")]
        public string complete { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Driver Name")]
        public virtual Driver drivr { get; set; }


        /// <summary>
        /// 
        /// </summary>

        [DataType(DataType.Text)]
        [Display(Name = "Details")]
        public string detailsProp { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Starting Address")]
        public string sAddressProp { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Ending Address")]
        public string eAddressProp { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Complete?")]
        public string completeProp { get; set; }

        public bool completeB { get; set; }
    }
}